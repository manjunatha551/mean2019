'use strict';

import {CountriesComponent} from './countries/countries.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('countries', {
    url: '/countries',
    name: 'countries',
    abstract: true,
    authenticate: 'user',
    template: require('./countries/countries.html'),
    // component: 'countries',
    controllerAs: '$ctrl',
    controller: CountriesComponent,
  });

  $stateProvider.state('countries.list', {
    url: '/list',
    name: 'countries.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'countries.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('countries.new', {
    url: '/new',
    name: 'countries.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'countries.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

    $stateProvider.state('countries.basic', {
      url: '/:countryId/basic',
      name: 'countries.basic',
      authenticate: 'user',
      resolve: {
        countryId: function($stateParams){
          return $stateParams.countryId;
        },
        country: function($http, $stateParams){
          return $http.get('/api/countries/'+$stateParams.countryId)
            .then(response => {
              return response.data;
            });
        }
      },
      template: require('./new/new.basic.html'),
      // component: 'countries.list',
      controllerAs: '$ctrl',
      controller: NewComponent,
    });

  $stateProvider.state('countries.info', {
    url: '/:countryId',
    name: 'countries.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      countryId: function($stateParams){
        return $stateParams.countryId;
      },
      country: function($http, $stateParams){
        return $http.get('/api/countries/'+$stateParams.countryId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'countries.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

  $stateProvider.state('countries.edit', {
    url: '/:countryId/update',
    name: 'countries.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      countryId: function($stateParams){
        return $stateParams.countryId;
      },
      country: function($http, $stateParams){
        return $http.get('/api/countries/'+$stateParams.countryId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'countries.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });

}
