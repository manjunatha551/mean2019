'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './countries.routes';

export default angular.module('mean2019App.countries', [uiRouter])
  .config(routing)
  .name;
