import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';
export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newemployee = {
    joiningdate:'',
    fname: '',
    mname:'',
    lname: '',
    primaryemail:'',
    secondaryemail: '',
    mobile:'',
    mobilenumber:'',
    dob:'',
    genderMale:'',
    genderFemale:'',
    genderTransgender:'',
    empId:'',
    fathername:'',
    panId:'',
    nationality:'',
    aadhar:'',
    passport:'',
    validfrom:'',
    validto:'',
    designation:'',
    departments:'',
    statusSingle:'',
    statusMarried:'',
    statusWidow:'',
    statusDivorce:'',
    SpouseName:'',
    CurrentAddress:'',
    PermanentAddress:'',
    Pos:'',
    pos:'',
    dob:'',
    tenth:{},
    tenthpercentage:'',
    tenthpassedout:'',
    intermediate:{},
    Specialization:'',
    Intermediatepercentage:'',
    Interpassedout:'',
    graduation:{},
    gradspeci:'',
    graduationpercentage:'',
    From:'',
    To:'',
    postgraduation:{},
    postspeci:'',
    postgraduationpercentage:'',
    pgfrom:'',
    pgto:'',
    pHD:{},
    pHDspec:'',
    pHDfrom:'',
    pHDto:'',
    pHDpercentage:'',
    gap:'',
    Howmanyyears:'',
    Experience:'',
    Fresher:'',
    companyname:'',
    Fromdate:'',
    Todate:'',
    designations:'',

  };
  designations = ['Senior HR','HR','Developer','Testing','Supporters','Maid','Accountant','Office boy','Recruiters'];
  designation = ['Senior HR','HR','Developer','Testing','Supporters','Maid','Accountant','Office boy','Recruiters'];
  // departments = ['Senior HR','HR','Developer','Testing','Supporters','Maid','Accountant','Office boy','Recruiters'];
  roles= ['Senior HR','HR','Developer','Testing','Supporters','Maid','Accountant','Office boy','Recruiters'];
  message = 'Add a new employee from here.';
  submitted=false;
  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }
  $onInit() {
    this.$http.get('/api/departments')
      .then(response => {
        this.departments = response.data;
      });
  }

  addEmployee(employeeForm) {
    if(employeeForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newemployee.fname) {
      this.$http.post('/api/employees',{
        joiningdate:this.newemployee.joiningdate,
      fname:this.newemployee.fname,
      mname:this.newemployee.mname,
      lname:this.newemployee.lname,
      primaryemail:this.newemployee.primaryemail,
      secondaryemail:this.newemployee.secondaryemail,
      mobile:this.newemployee.mobile,
      mobilenumber:this.newemployee.mobilenumber,
      dob:this.newemployee.dob,
      genderMale:this.newemployee.genderMale,
      genderFemale:this.newemployee.genderFemale,
      genderTransgender:this.newemployee.genderTransgender,


      })
      .then( response => {
        this.$state.go('employees.basic', {employeeId: response.data._id});
      }).catch( err => {
        console.log('oops');
      });
    }
  }


    updateEmployee(employeeForm, employee) {
      // if(employeeForm.$invalid){
      //   this.submitted=true;
      //   return;
      // }
      if(this.newemployee.empId) {
        this.$http.put('/api/employees/'+employee._id,{
        empId:this.newemployee.empId,
        fathername:this.newemployee.fathername,
        panId:this.newemployee.panId,
        nationality:this.newemployee.nationality,
        aadhar:this.newemployee.aadhar,
        passport:this.newemployee.passport,
        validfrom:this.newemployee.validfrom,
        validto:this.newemployee.validto,
        designation:this.newemployee.designation,
        departments:this.newemployee.department,
        statusSingle:this.newemployee.statusSingle,
        statusMarried:this.newemployee.statusMarried,
        statusWidow:this.newemployee.statusWidow,
        statusDivorce:this.newemployee.statusDivorce,
        spouse:this.newemployee.SpouseName,
        CurrentAddress:this.newemployee.CurrentAddress,
        Pos:this.newemployee.Pos,
        PermanentAddress:this.newemployee.PermanentAddress,
        pos:this.newemployee.pos,
        education:{
          gap:this.newemployee.gap,
          tenth: {
            tenth:this.newemployee.tenth.tenth,
            Insname: this.newemployee.Insname,
            tenthpassedout:this.newemployee.tenthpassedout,
            tenthpercentage: this.newemployee.tenthpercentage,
          },
          intermediate: {
            intermediate:this.newemployee.intermediate.intermediate,
            insname: this.newemployee.insname,
            Specialization:this.newemployee.Specialization,
            Interpassedout: this.newemployee.Interpassedout,
            Intermediatepercentage: this.newemployee.Intermediatepercentage,
          },
          graduation: {
            graduation:this.newemployee.graduation.graduation,
            Uniname: this.newemployee.Uniname,
            gradspeci:this.newemployee.gradspeci,
            From:this.newemployee.From,
            To:this.newemployee.To,
            graduationpercentage: this.newemployee.graduationpercentage,
          },
          postgraduation:{
            postgraduation:this.newemployee.postgraduation.postgraduation,
            uniname:  this.newemployee.uniname,
            postspeci:this.newemployee.postspeci,
            pgfrom: this.newemployee.pgfrom,
            pgto: this.newemployee.pgto,
            postgraduationpercentage: this.newemployee.postgraduationpercentage,
          },
          pHD: {
            pHD:this.newemployee.pHD.pHD,
            pHDspec:this.newemployee.pHDspec,
            universityname: this.newemployee.universityname,
            pHDfrom: this.newemployee.pHDfrom,
            pHDto: this.newemployee.pHDto,
            pHDpercentage: this.newemployee.pHDpercentage,
        },
      },
          Howmanyyears:this.newemployee.Howmanyyears,
        employment: {

            Experience:this.newemployee.Experience,
            Fresher:this.newemployee.Fresher,
            companyname: this.newemployee.companyname,
            Fromdate: this.newemployee.Fromdate,
            Todate: this.newemployee.Todate,
            designations: this.newemployee.designations,

}
        })
        .then( response => {
          this.$state.go('employees.info', {employeeId: response.data._id});
        }).catch( err => {
          console.log('oops');
        });
      }
    }


  resetForm(){
    this.newemployee = {
      joiningdate:'',
      fname: '',
      mname:'',
      lname: '',
      primaryemail:'',
      secondaryemail: '',
      mobile:'',
      mobilenumber:'',
      dob:'',
      genderMale:'',
      genderFemale:'',
      genderTransgender:'',
      empId:'',
      fathername:'',
      panId:'',
      nationality:'',
      aadhar:'',
      passport:'',
      validfrom:'',
      validto:'',
      designation:'',
      department:'',
      statusSingle:'',
      statusMarried:'',
      statusWidow:'',
      statusDivorce:'',
      SpouseName:'',
      CurrentAddress:'',
      PermanentAddress:'',
      Pos:'',
      pos:'',
      dob:'',
      tenth:'',
      tenthpercentage:'',
      tenthpassedout:'',
      intermediate:'',
      Specialization:'',
      Intermediatepercentage:'',
      Interpassedout:'',
      graduation:'',
      gradspeci:'',
      graduationpercentage:'',
      From:'',
      To:'',
      postgraduation:'',
      postspeci:'',
      postgraduationpercentage:'',
      pgfrom:'',
      pgto:'',
      pHD:'',
      pHDspec:'',
      pHDfrom:'',
      pHDto:'',
      pHDpercentage:'',
      gap:'',
      Howmanyyears:'',
      Experience:'',
      Fresher:'',
      companyname:'',
      Fromdate:'',
      Todate:'',
      designations:'',

    };
  }
}
