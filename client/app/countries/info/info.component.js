import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    // 'ngInject';
    this.modal=Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    // this.$http.get('/api/employees')
    //   .then(response => {
    //     this.employees = response.data;
    //   }).catch(err => {
    //
    //   })
  }

  getConvertDate(date){
    return new Date(date);
  }

  deleteEmployee(employee) {
    var deleteConfirmationModal = this.modal.confirm.delete((employee)=>{
      this.$http.delete('/api/employees/'+employee._id,{
      }).then(response=>{
        var modalAlert = this.modal.confirm.modalAlert(response, employee.name);
        this.$state.go('employees.list');
      }).catch(err =>{
        console.log('employee not deleted');
      });
    })
    deleteConfirmationModal(employee.name, employee);
  }

}
