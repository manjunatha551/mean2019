import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newclient = {
    name: '',
    mobilenumber:'',
    pincode: '',
    locality:'',
    address:'',
    city: '',
    state:'',
    landmark:'',
    alternatenumber: ''


  };

  states  = ['Andhrapradesh','Karnataka','Tamilnadu'];
  message = 'Add a new Addresses from here.';

  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {

  }

  addaddress(addressForm) {
    if(addressForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newaddress.name) {
      this.$http.post('/api/addresses', this.newaddress)
      .then( response => {
        this.$state.go('addresses.info', {addressId: response.data._id});
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }

  resetForm(){
    this.newaddress = {
      name: '',
      mobilenumber:'',
      pincode: '',
      locality:'',
      address:'',
      city: '',
      state:'',
      landmark:'',
      alternatenumber: '',

    };
  }

}
