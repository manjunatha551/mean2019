'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './addresses.routes';

export default angular.module('mean2019App.addresses', [uiRouter])
  .config(routing)
  .name;
