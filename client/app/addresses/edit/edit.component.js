import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  updateAddress(addressForm, address){
    if(addressForm.$invalid){
      this.submitted=true;
      return;
    }
    if( address.name){
    console.log("hi"+address);
    this.$http.put('/api/addresses/'+address._id,{
      name: address.name,
      mobilenumber:address.mobilenumber,
      pincode: address.pincode,
      locality:address.locality,
      address:address.address,
      city: address.city,
      state:address.state,
      landmark:address.landmark,
      alternatenumber: address.alternatenumber
    }).then(res =>{
      this.$state.go('addresses.info',{addressId:res.data._id},{reload:true});
    })
  }
}
  deleteAddress(address) {
    console.log('deleting...');
    console.log(address);
    this.$http.delete(`/api/addresses/${address._id}`);
    this.$state.go('addresses.list');
  }

}
