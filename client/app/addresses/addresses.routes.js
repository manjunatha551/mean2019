'use strict';

import {AddressesComponent} from './addresses/addresses.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('addresses', {
    url: '/addresses',
    name: 'addresses',
    abstract: true,
    authenticate: 'user',
    template: require('./addresses/addresses.html'),
    // component: 'addresses',
    controllerAs: '$ctrl',
    controller: AddressesComponent,
  });

  $stateProvider.state('addresses.list', {
    url: '/list',
    name: 'addresses.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'addresses.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('addresses.new', {
    url: '/new',
    name: 'addresses.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'addresses.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  
  $stateProvider.state('addresses.info', {
    url: '/:addressId',
    name: 'addresses.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      addressId: function($stateParams){
        return $stateParams.addressId;
      },
      country: function($http, $stateParams){
        return $http.get('/api/addresses/'+$stateParams.addressId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'addresses.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

  $stateProvider.state('addresses.edit', {
    url: '/:addressId/update',
    name: 'addresses.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      addressId: function($stateParams){
        return $stateParams.addressId;
      },
      country: function($http, $stateParams){
        return $http.get('/api/addresses/'+$stateParams.addressId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'addresses.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });

}
