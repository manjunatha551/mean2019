'use strict';

import angular from 'angular';

export default angular.module('mean2019App.constants',  ['ngQuill'])
  .constant('appConfig', require('../../server/config/environment/shared'))
  .name;
