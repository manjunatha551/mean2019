import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newproduct = {
    name: '',
    description: '',
    quantity:'',
    size:'',
    model:'',
    price:'',

  };

  // industries  = ['Accounting','Architecture & planning','Broadcast Media','Business Supplies and Equipment',
  //               'Chemicals','Computer and Network Security','Financial Services','Government Relations','Information Services',
  //               'Information Technology and Services','Marketing and Advertising','Managemnet Consulting','Nanotechnology','Product','Research','Telecommunications','Utilities',
  //               'Wholesale'];
  // experiences = ['0-1 Years','1-2 Years','2-3 Years','3-4 Years','More...'];
  // roles = ['Fullstack','UX','UI Developer','Development','HR Recutiment','Business Development','Supportive Role'];
  // types = ['Fulltime','Parttime'];
  // keywords = ['java','oracle'];
  message = 'Product Info Form';

  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  addproduct(productForm) {
    if(productForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newproduct.title) {
      this.$http.post('/api/products', this.newproduct)
      .then( response => {
        //this.$state.go('pawns.info', {name: response.data.name});
        this.$state.go('products.info', {productId: response.data._id});
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }
  // addproduct() {
  //   console.log(this.newproduct)
  //   if(this.newproduct.title) {
  //     this.$http.post('/api/products', this.newproduct)
  //     .then( response => {
  //       //this.$state.go('pawns.info', {name: response.data.name});
  //       this.$state.go('products.info', {productId: response.data._id});
  //       this.resetForm();
  //     }).catch( err => {
  //       console.log('oops');
  //     });
  //
  //   }
  // }

  resetForm(){
    this.newproduct = {
      name: '',
      description: '',
      quantity:'',
      size:'',
      model:'',
      price:'',
    };
  }

}
