'use strict';

import {ProductComponent} from './products/products.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('products', {
    url: '/products',
    name: 'products',
    abstract: true,
    authenticate: 'user',
    template: require('./products/products.html'),
    // component: 'pawns',
    controllerAs: '$ctrl',
    controller: ProductComponent,
  });

  $stateProvider.state('products.list', {
    url: '/list',
    name: 'products.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'pawns.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('products.new', {
    url: '/new',
    name: 'products.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'pawns.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('products.info', {
    url: '/:productId',
    name: 'products.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {

      productId: function($stateParams){
        return $stateParams.productId;
      },
      product: function($http, $stateParams){
        return $http.get('/api/products/'+$stateParams.productId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'pawns.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
  $stateProvider.state('products.edit', {
    url: '/:productId/edit',
    name: 'products.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      productId: function($stateParams){
        return $stateParams.productId;
      },
      product: function($http, $stateParams){
        return $http.get('/api/products/'+$stateParams.productId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
