'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './products.routes';

export default angular.module('mean2019App.products', [uiRouter])
  .config(routing)
  .name;
