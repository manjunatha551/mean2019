import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    // 'ngInject';
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  // deleteproduct(product) {
  //   console.log('deleting...');
  //   console.log(product);
  //   this.$http.delete(`/api/products/${product._id}`);
  //   this.$state.go('products.list');
  // }
  deleteproduct(product) {
    var deleteConfirmationModal = this.modal.confirm.delete((product) => {
    this.$http.delete('/api/products/'+product._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, product.name);
      this.$state.go('products.list');
    }).catch(err => {
      console.log('product not deleted');
    });
  })
  deleteConfirmationModal(product.name, product);
  }

}
