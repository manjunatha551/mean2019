import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  updateproduct(productForm,product){
    if(productForm.$invalid){
      this.submitted=true;
      return;
    }
    if(product.title){
      console.log("hi"+product);
      this.$http.put('/api/products/'+product._id,{
          title:product.title,
          description:product.description,
          keywords:product.keywords,
          annual:product.annual,
          type:product.type,
          nov:product.nov,
          np:product.np,
          experience:product.experience,
          industry:product.industry,
          role:product.role,
          location:product.role,
          area:product.area
      }).then(res =>{
        this.$state.go('products.info',{productId:res.data._id},{reload:true});
      })
  }
}

  deleteproduct(product) {
    console.log('deleting...');
    console.log(product);
    this.$http.delete(`/api/products/${product._id}`);
    this.$state.go('products.list');
  }

}
