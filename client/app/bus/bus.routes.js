'use strict';

import {BusComponent} from './bus/bus.component.js';
import {ListComponent} from './list/list.component';
// import {NewComponent} from './new/new.component';
// import {InfoComponent} from './info/info.component';
// import {EditComponent} from './edit/edit.component';
export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('bus', {
    url: '/bus',
    name: 'bus',
    abstract: true,
    authenticate: 'user',
    template: require('./bus/bus.html'),
    // component: 'buses',
    controllerAs: '$ctrl',
    controller: BusComponent,
  });
  $stateProvider.state('bus.list', {
    url: '/list',
    name: 'bus.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'buses.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });
  // $stateProvider.state('buses.new', {
  //   url: '/new',
  //   name: 'buses.new',
  //   authenticate: 'user',
  //   template: require('./new/new.html'),
  //   // component: 'buses.new',
  //   controllerAs: '$ctrl',
  //   controller: NewComponent,
  // });
  //
  //   $stateProvider.state('buses.basic', {
  //     url: '/:busesId/basic',
  //     name: 'buses.basic',
  //     authenticate: 'user',
  //     resolve: {
  //       busesId: function($stateParams){
  //         return $stateParams.busesId;
  //       },
  //       country: function($http, $stateParams){
  //         return $http.get('/api/buses/'+$stateParams.busId)
  //           .then(response => {
  //             return response.data;
  //           });
  //       }
  //     },
  //     template: require('./new/new.basic.html'),
  //     // component: 'buses.list',
  //     controllerAs: '$ctrl',
  //     controller: NewComponent,
  //   });
  //
  // $stateProvider.state('buses.info', {
  //   url: '/:busesId',
  //   name: 'buses.info',
  //   authenticate: 'user',
  //   template: require('./info/info.html'),
  //   resolve: {
  //     busesId: function($stateParams){
  //       return $stateParams.busesId;
  //     },
  //     country: function($http, $stateParams){
  //       return $http.get('/api/buses/'+$stateParams.busesId)
  //         .then(response => {
  //           return response.data;
  //         });
  //     },
  //   },
  //   component: 'buses.info',
  //   controllerAs: '$ctrl',
  //   controller: InfoComponent,
  // });
  //
  // $stateProvider.state('buses.edit', {
  //   url: '/:busesId/update',
  //   name: 'buses.edit',
  //   authenticate: 'user',
  //   template: require('./edit/edit.html'),
  //   resolve: {
  //     busesId: function($stateParams){
  //       return $stateParams.busesId;
  //     },
  //     country: function($http, $stateParams){
  //       return $http.get('/api/buses/'+$stateParams.busesId)
  //         .then(response => {
  //           return response.data;
  //         });
  //     },
  //   },
  //   // component: 'buses.info',
  //   controllerAs: '$ctrl',
  //   controller: EditComponent,
  // });
}
