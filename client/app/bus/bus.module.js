'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './bus.routes';

export default angular.module('mean2019App.bus', [uiRouter])
  .config(routing)
  .name;
