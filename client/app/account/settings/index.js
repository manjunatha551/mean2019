'use strict';

import angular from 'angular';
import SettingsController from './settings.controller';

export default angular.module('mean2019App.settings', [])
  .controller('SettingsController', SettingsController)
  .name;
