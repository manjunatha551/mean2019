'use strict';

import angular from 'angular';
import routes from './admin.routes';
import AdminController from './admin.controller';

export default angular.module('mean2019App.admin', ['mean2019App.auth', 'ui.router'])
  .config(routes)
  .controller('AdminController', AdminController)
  .name;
