'use strict';

import angular from 'angular';
import {
  UtilService
} from './util.service';

export default angular.module('mean2019App.util', [])
  .factory('Util', UtilService)
  .name;
