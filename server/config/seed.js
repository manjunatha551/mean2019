/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {
    Thing.find({}).remove()
      .then(() => {
        let thing = Thing.create({
          name: 'Development Tools',
          info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
                + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
                + 'Stylus, Sass, and Less.'
        }, {
          name: 'Server and Client integration',
          info: 'Built with a powerful and fun stack: MongoDB, Express, '
                + 'AngularJS, and Node.'
        }, {
          name: 'Smart Build System',
          info: 'Build system ignores `spec` files, allowing you to keep '
                + 'tests alongside code. Automatic injection of scripts and '
                + 'styles into your app.html'
        }, {
          name: 'Modular Architecture',
          info: 'Best practice client and server structures allow for more '
                + 'code reusability and maximum scalability'
        }, {
          name: 'Optimized Build Generation',
          info: 'Build process packs up your templates as a single JavaScript '
                + 'payload, minifies your scripts/css/images, and rewrites asset '
                + 'names for caching.'
        }, {
          name: 'Deployment Ready',
          info: 'Easily deploy your app to Heroku or Openshift with the heroku '
                + 'and openshift subgenerators'
        });
        return thing;
      })
      .then(() => console.log('finished populating things'))
      .catch(err => console.log('error populating things', err));

    // User.find({}).remove()
    //   .then(() => {
    //     User.create(
    //       {
    //       provider: 'local',
    //       name: 'Bharath Chetty',
    //       email: 'bharath.chetty@graylocus.com',
    //       password: 'mean2019',
    //       role: 'admin'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Admin',
    //       email: 'superuser@graylocus.com',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Admin',
    //       email: 'superuser@graylocus.com',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Poojitha Patrapati',
    //       email: 'poojitha.patrapati@niranta.in',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Sainath Middi',
    //       email: 'sainath.middi@niranta.in',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Pavan Raju',
    //       email: 'pavankumarraju.c@niranta.in',
    //       password: 'mean2019'
    //     },
    //     {
    //       providername: 'local',
    //       role: 'admin',
    //       name: 'Sunil Atkur',
    //       email: 'sunil.atkur@niranta.in',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Manjunath V',
    //       email: 'manjunath.v@niranta.in',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Kulasekhar V',
    //       email: 'kulasekhar.v@niranta.in',
    //       password: 'mean2019'
    //     },
    //     {
    //       provider: 'local',
    //       role: 'admin',
    //       name: 'Srinivas M',
    //       email: 'srinivas.m@niranta.in',
    //       password: 'mean2019'
    //     }
    //   )
    //     .then(() => console.log('finished populating users'))
    //     .catch(err => console.log('error populating users', err));
    //   });
  }
}
