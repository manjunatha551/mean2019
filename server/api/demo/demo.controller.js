

export function getAllRecords(req, res){
  // the following line sends  a text reponse
  //return res.status(200).send('send all records here');
  // the following line sends  an HTML reponse
  return res.status(200).send('<h1 style="color:red;">Here is your list</h1>');
}

export function getRecordById(req, res){
  var recId  = req.params.recordId;
  // the following line sends  a text reponse
  //return res.status(200).send(' here is your record '+ recId);
  // the following line sends  a json reponse
  return res.status(200).json({'name':  'some record', 'recId': recId});
}
