'use strict';

import {Router} from 'express';
import * as controller from './demo.controller';
// import * as auth from '../../auth/auth.service';

var router = new Router();


//the following route uses a function as the second arguments
router.get('/call', function(req, res){
  return res.status(200).send('local call');
});

//the following routes use a function call from the controller 
router.get('/list',controller.getAllRecords);
router.get('/:recordId',controller.getRecordById);



// router.delete('/:id', auth.hasRole('admin'), controller.destroy);
// router.get('/me', auth.isAuthenticated(), controller.me);
// router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
// router.get('/:id', auth.isAuthenticated(), controller.show);
// router.post('/', controller.create);

module.exports = router;
