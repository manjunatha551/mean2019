'use strict';

import mongoose from 'mongoose';

var CountrySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  code: {
    type: String,
    required: true
  },
  callingCode: {
    type: Number
  },
  currency : {
    symbol: String,
    code: String,
    name: String
  }

});

export default mongoose.model('Country', CountrySchema);
