'use strict';

var express = require('express');
var controller = require('./countries.controller');

var router = express.Router();
import * as auth from '../../auth/auth.service';

router.get('/', auth.isAuthenticated(), controller.getAllCountries);
router.get('/', controller.getAllCountries);
router.get('/raw', controller.getRawData);
router.get('/bulkcreate', controller.bulkCreate);
router.get('/:countryId', controller.getCountryById);
router.get('/:countryName', controller.getCountryByName);
router.post('/', controller.addCountry);
router.post('/upload', controller.uploadCountries);
router.put('/:countryId', controller.updateCountry);
router.patch('/:countryId', controller.patch);
router.delete('/:countryId', controller.deleteCountry);

module.exports = router;
