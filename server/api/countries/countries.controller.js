/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/countries              ->  index
 * POST    /api/countries              ->  create
 * GET     /api/countries/:id          ->  show
 * PUT     /api/countries/:id          ->  upsert
 * PATCH   /api/countries/:id          ->  patch
 * DELETE  /api/countries/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Country from './country.model';
import countries from '../../config/countries.json';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {myData
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Countries
export function getAllCountries(req, res) {
  console.log(req.user.name);
  return Country.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Country by Id from the DB
export function getCountryById(req, res) {
  return Country.findById(req.params.countryId).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of countries by name from the DB
export function getCountryByName(req, res) {
  return Country.find({name : req.params.countryName}).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Country in the DB
export function addCountry(req, res) {
  return Country.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

export function uploadCountries(req, res){
  var countrieslist=req.body.countries;
  var promises=[];
  if (countrieslist) {
  for (var i = 0; i < countrieslist.length; i++) {
    var c= countrieslist[i];
    var p= Country.create(c);
    promises.push(p);
  }
    return Promise.all(promises)
    .then(records=>{
      return res.status(201).json(records);
    })
    .catch(handleError(res));
  }

}
export function bulkCreate(req, res){
  return Country.insertMany(countries)
    .then(function(docs) {
      return res.status(201).json(docs);
         // do something with docs
    })
    .catch(function(err) {
      return res.status(500).json({'message':err});
        // error handling here
    });
}
export function getRawData(req, res){
  return res.status(200).json(countries);

}
// Upserts the given Country in the DB at the specified ID
export function updateCountry(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Country.findOneAndUpdate({_id: req.params.countryId}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Country in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Country.findById(req.params.countryId).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Country from the DB
export function deleteCountry(req, res) {
  return Country.findById(req.params.countryId).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
