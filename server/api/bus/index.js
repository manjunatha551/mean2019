'use strict';
import * as auth from '../../auth/auth.service';



var express = require('express');
var controller = require('./bus.controller');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.getAllBuses);
// router.get('/:movieId', controller.getbusById);
router.post('/', controller.createbus);
// router.put('/:movieId', controller.updatebus);
// router.patch('/:movieId', controller.patch);
// router.delete('/:movieId', controller.deletebus);

module.exports = router;
