/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/movies              ->  index
 * POST    /api/movies              ->  create
 * GET     /api/movies/:busId          ->  show
 * PUT     /api/movies/:busId          ->  upsert
 * PATCH   /api/movies/:busId          ->  patch
 * DELETE  /api/movies/:busId          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import bus from './bus.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*valbusIdate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of buss
export function getAllBuses(req, res) {
  return bus.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single bus from the DB
export function getbusById(req, res) {
  console.log(req.user);
  return bus.findById(req.params.busId).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new bus in the DB
export function createbus(req, res) {
  return bus.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given bus in the DB at the specified ID
export function updatebus(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return bus.findOneAndUpdate({_id: req.params.busId}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValbusIdators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing bus in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return bus.findById(req.params.busId).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a bus from the DB
export function deletebus(req, res) {
  return bus.findById(req.params.busId).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
