'use strict';

import mongoose from 'mongoose';

var busSchema = new mongoose.Schema({
  busTitle: {
    type: String,
    // required: true
  },
  busType: {
    type: String,
    // required: true
  },
  rating: {
    // required: true,
    type: String,
    enum: ['1','2', '3', '4', '5']
  },
  franchise: [String],
  company:{
    type: String,
  }
  });

export default mongoose.model('bus', busSchema);
