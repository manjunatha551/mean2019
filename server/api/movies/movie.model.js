'use strict';

import mongoose from 'mongoose';

var MovieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  year: {
    type: Number,
    required: true
  },
  rating: {
    required: true,
    type: String,
    enum: ['1','2', '3', '4', '5']
  },
  directors: [String],
  musicdirectors: [String],
  olanguage:{
    type: String,
    enum: [ 'hindi', 'telugu', 'tamizh', 'kannada', 'english', 'malayalam']
  },
  hit: Boolean
});

export default mongoose.model('Movie', MovieSchema);
