'use strict';

var express = require('express');
var controller = require('./movies.controller');

var router = express.Router();

router.get('/', controller.getAllMovies);
router.get('/:movieId', controller.getMovieById);
router.post('/', controller.createMovie);
router.put('/:movieId', controller.updateMovie);
router.patch('/:movieId', controller.patch);
router.delete('/:movieId', controller.deleteMovie);

module.exports = router;
