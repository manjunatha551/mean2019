'use strict';

import mongoose from 'mongoose';

var ProductSchema = new mongoose.Schema({
  productId:Number,
  name: String,
  description:String,
  quantity: Number,
  size:Number,
  model: Number,
  price: Number,
});

export default mongoose.model('Product', ProductSchema);
