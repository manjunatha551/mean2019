'use strict';

import mongoose from 'mongoose';

var AddressSchema = new mongoose.Schema({
  name: String,
  mobilenumber: Number,
  pincode: Number,
  locality: String,
  addresses: String,
  city: String,
  state: String,
  landmark: String,
  alternatenumber: Number
});

export default mongoose.model('Address', AddressSchema);
